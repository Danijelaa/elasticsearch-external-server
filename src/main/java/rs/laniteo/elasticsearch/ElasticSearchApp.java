package rs.laniteo.elasticsearch;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class ElasticSearchApp extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(ElasticSearchApp.class, args);
	}

	

}

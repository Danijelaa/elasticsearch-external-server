package rs.laniteo.elasticsearch;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

/*import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rs.laniteo.elasticsearch.model.Student;
import rs.laniteo.elasticsearch.repository.StudentRepository;

@Component
public class TestData {

	@Autowired
	StudentRepository sr;
	@PostConstruct
	public void init() throws IOException{
		System.out.println("Jesi, it's me.");
		
		/*Settings settings = Settings.settingsBuilder()
    	        .put("cluster.name", "my-application").build();
    	Client client = TransportClient.builder().settings(settings).build();*/
		
		/*Client client=null;
		BulkRequestBuilder requestBuilder;
		try {
		    client = TransportClient.builder().settings(Settings.builder().put("cluster.name", "my-application").put("node.name","node1")).build().addTransportAddress(
		    new InetSocketTransportAddress(InetAddress.getByName(""), 9300));
		    requestBuilder = (client).prepareBulk();
		} 
		catch (Exception e) {
		}*/
		//TransportClient client = new PreBuiltTransportClient(Settings.EMPTY);
		/*Settings settings = Settings.builder().put("cluster.name", "my-application").build();
		TransportClient client = new PreBuiltTransportClient(settings).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"),9300));
		Settings settings = Settings.settingsBuilder()
		        .loadFromSource("./src/main/resources/application.properties").build();
		Client client = TransportClient.builder().settings(settings).build();
		
		Map<String, Object> jsonMap = new HashMap<>();
		jsonMap.put("name", "Danijelaaa");
		IndexRequest indexRequest = new IndexRequest("students", "doc", "1")
		        .source(jsonMap);
		IndexResponse response = client.prepareIndex("students", "_doc")
                .setSource(jsonBuilder()
                        .startObject()
                        .field("id", 3)
                        .field("name", "Dacaaaa")
                        .endObject()
                )
                .get();*/
		//IndexResponse indexResponse = client.index(indexRequest);
		Student s1=new Student();
		s1.setId("1");
		s1.setName("Danijela");
		sr.save(s1);
		
		Student s2=new Student();
		s2.setId("2");
		s2.setName("Daca");
		sr.save(s2);
		System.out.println("Zavrseno.");
	}
}

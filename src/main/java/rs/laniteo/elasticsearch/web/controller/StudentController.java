package rs.laniteo.elasticsearch.web.controller;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/*import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;*/
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.laniteo.elasticsearch.model.Student;
import rs.laniteo.elasticsearch.repository.StudentRepository;

@RestController
@RequestMapping(value="/students")
public class StudentController {

	@Autowired
	StudentRepository sr;
	
	@RequestMapping(method=RequestMethod.GET, value="/all")
	ResponseEntity<List<Student>> getAll(){
		List<Student> students=new ArrayList<Student>();
		Iterator<Student> it=sr.findAll().iterator();
		while(it.hasNext()){
			students.add(it.next());
		}
		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
	}
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	ResponseEntity<Student> getOne(@PathVariable String id){
		Student s=sr.findOne(id);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}
	@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<?> getByName(@RequestParam String name){
		/*Settings settings = Settings.settingsBuilder()
                .put("cluster.name", "my-application").build();

        Client client = TransportClient.builder().settings(settings).build()
                .addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress("127.0.0.1", 9300)))
                .addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress("127.0.0.1", 9301)));*/
		/*Node node=NodeBuilder.nodeBuilder()
			    .settings(Settings.builder()
			            .put("path.home", "./data/elasticsearch")
			        .node());
		//Node node = NodeBuilder.nodeBuilder().client(true).clusterName("my-application").node();
		Client client = node.client();*/
		QueryBuilder qb = QueryBuilders.matchQuery(
			    "name",                  
			    name   
			);
		/*SearchResponse response = client.prepareSearch("index") //
			    .setQuery(qb) // Query
			    .execute().actionGet();*/
		
		Student s=sr.findByName(name);
		
		return new ResponseEntity<>(s, HttpStatus.OK);
	}
	@RequestMapping(method=RequestMethod.POST)
	ResponseEntity<Student> getOne(@RequestBody Student s){
		sr.save(s);
		return new ResponseEntity<>(s, HttpStatus.OK);
	}
	
}
